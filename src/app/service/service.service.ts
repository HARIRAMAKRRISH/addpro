import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UserDetails } from '../model/userDetails';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  userURL:any = environment.capstone;

  constructor(private http:HttpClient) { }

  isLoggedIn(): boolean {
    return !!localStorage.getItem('name');
  }

  public registerUser(user: UserDetails): Observable<object> {
    console.log(user, 'us');
    return this.http.post(`${this.userURL}/addUser`, user);
  }

  public loginUser(email:string,password:string): Observable<object> {
    console.log(email, password);
    return this.http.get(`${this.userURL}/userLogin/${email}/${password}`);
  }

}