import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcrudComponent } from './procrud.component';

describe('ProcrudComponent', () => {
  let component: ProcrudComponent;
  let fixture: ComponentFixture<ProcrudComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProcrudComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcrudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
