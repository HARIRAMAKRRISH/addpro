import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup } from '@angular/forms'; 
import { ApiService } from '../../shared/api'
import { productmodel } from './productmodel';
@Component({
  selector: 'app-procrud',
  templateUrl: './procrud.component.html',
  styleUrls: ['./procrud.component.scss']
})
export class ProcrudComponent implements OnInit {

  formValue !: FormGroup;
  BookModelObj : productmodel =new productmodel();
  bookData !: any
  showAdd !: boolean;
  showUpdate !:boolean;

  constructor(private formbuilder:FormBuilder,private apiservice:ApiService ) { }

  ngOnInit(): void {
    this.formValue = this.formbuilder.group({
      title :[''],
      category :[''],
      description :[''],
      image :[' '],
      price:['']

    })
    this.getAllBooks();
  }

  cilckAddBook()
  {
    this.formValue.reset();
    this.showAdd=true;
    this.showUpdate=false;
  }
  
  postBookData()
  {
    this.BookModelObj.title = this.formValue.value.title;
    this.BookModelObj.category = this.formValue.value.category;
    this.BookModelObj.description = this.formValue.value.description;
    this.BookModelObj.image = this.formValue.value.image;
    this.BookModelObj.price = this.formValue.value.price;

    this.apiservice.postBook(this.BookModelObj)
    .subscribe(res=>{
      console.log(res)
      alert("Book Added Successfully")
      let ref = document.getElementById('cancel')
      ref?.click();
      this.formValue.reset();
      this.getAllBooks();
    },
    err=>{
      alert("Something went wrong")
    }
    )
    
  }

  getAllBooks()
  {
    this.apiservice.getBook()
    .subscribe(res=>
      {
        this.bookData = res;

      })
  }

  deleteBooks(row : any)
  {
    this.apiservice.DeleteBook(row.id)
    .subscribe(res=>{
      alert('Book Deleted')
      this.getAllBooks();
    })

  }

  editBook(row: any)
  {
    this.formValue.reset();
    this.showAdd=false;
    this.showUpdate=true;
    this.BookModelObj.id= row.id;
    this.formValue.controls['title'].setValue(row.title)
    this.formValue.controls['category'].setValue(row.category)
    this.formValue.controls['description'].setValue(row.description)
    this.formValue.controls['image'].setValue(row.image)
    this.formValue.controls['price'].setValue(row.price)
  }

  updateBook()
  {
    this.BookModelObj.title = this.formValue.value.title;
    this.BookModelObj.category = this.formValue.value.category;
    this.BookModelObj.description = this.formValue.value.description;
    this.BookModelObj.image = this.formValue.value.image;
    this.BookModelObj.price = this.formValue.value.price;
    this.apiservice.UpdateBook(this.BookModelObj,this.BookModelObj.id)
    .subscribe(res=> {
      alert('update Successful')
      let ref = document.getElementById('cancel')
      ref?.click();
      this.formValue.reset();
      this.getAllBooks();
    })

  }

  

}
